import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as bankSelectors from 'state/bank/selectors';
import * as appActions from 'state/app/actions';
import * as bankActions from 'state/bank/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from './i18n';

const Container = styled.div`
  text-align: center;

  .header {
    font-weight: 600;
    font-size: 32px;
  }

  .description {
    margin: 16px auto;

    .fixed-description {
      font-size: 48px;
      font-weight: 600;
    }

    input {
      font-size: 32px;
      font-weight: 600;
      text-align: center;
    }
  }

  .other-item {
    &:before {
      content: '+ ';
    }

    color: ${props => props.theme.colors.secondary};
  }

  table {
    text-align: left;
    width: auto;
    margin: 32px auto;

    tr {
      &:last-child {
        background-color: ${props => props.theme.colors.mainLight};
      }

      td {
        &:first-child {
          padding-right: 32px;
        }

        border-bottom: 1px dotted #AAA;
        padding: 8px;
      }
    }
  }
`;
Container.displayName = 'PurchaseModalContainer';

const PurchaseModal = ({
  itemName, close, afterPurchase, payloadData, otherItems,
}) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const price = useSelector(state => bankSelectors.selectBuyableItemPrice(state, itemName));
  const otherprices = useSelector(
    state => bankSelectors.selectBuyableItemPriceBulk(state, otherItems),
  );

  const [purchasing, setPurchasing] = useState(false);

  const transfer = useCallback(async () => {
    try {
      setPurchasing(true);

      // eslint-disable-next-line no-restricted-syntax
      for (const item of otherItems) {
        if (item.length) {
          // eslint-disable-next-line no-await-in-loop
          await bankActions.createPurchase({ item });
        }
      }

      const payload = { item: itemName, payload: payloadData };
      const transaction = await bankActions.createPurchase(payload);

      if (afterPurchase) afterPurchase(transaction);

      dispatch(appActions.addToast(t('Purchase successful')));
      close();
    } catch (error) {
      setPurchasing(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, t, itemName, close, afterPurchase, payloadData, otherItems]);

  const actions = [];
  if (!purchasing) {
    actions.push();
  }

  return (
    <Modal
      title={t('Purchase')}
      onCancel={close}
      actions={[
        <Button key="purchase-confirm" loading={purchasing} onClick={transfer} disabled={!price}>{t('Purchase')}</Button>,
      ]}
    >
      <Container>
        <div className="header">{t(`${itemName}.name`)}</div>

        <div className="description">{t(`${itemName}.description`)}</div>

        {otherItems.map(oi => <div className="other-item">{t(`${oi}.name`)}</div>)}

        <table>
          <tbody>
            <tr>
              <td>{t('Value')}</td>
              <td>
                <strong>{price + otherprices}</strong>
                {' '}
                <span>sades</span>
              </td>
            </tr>
          </tbody>
        </table>
      </Container>
    </Modal>
  );
};

PurchaseModal.propTypes = {
  itemName: PropTypes.string.isRequired,
  otherItems: PropTypes.arrayOf(PropTypes.string),
  close: PropTypes.func.isRequired,
  afterPurchase: PropTypes.func,
  payloadData: PropTypes.shape({}),
};

PurchaseModal.defaultProps = {
  afterPurchase: null,
  payloadData: {},
  otherItems: [],
};

export default PurchaseModal;
