import styled from 'styled-components';

import colors from 'utils/css/colors';

const StepCounter = styled.span`
  color: ${colors.grey};
  font-size: 14px;
  text-align: center;
  display: block;
  margin: 0 0 16px;
`;

export default StepCounter;
