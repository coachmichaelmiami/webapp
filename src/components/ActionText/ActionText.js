import styled from 'styled-components';

import colors from 'utils/css/colors';

const ActionText = styled.div`
  color: ${colors.red};
  font-size: 16px;
  font-weight: 500;
  text-transform: uppercase;
`;

export default ActionText;
