export { default as TabsList } from './TabsList';
export { default as TabsWrapper } from './TabsWrapper';
export { default as TabsContent } from './TabsContent';
