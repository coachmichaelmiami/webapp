import styled from 'styled-components';
import PropTypes from 'prop-types';

import colors from 'utils/css/colors';

const normalBG = '255, 255, 255';
const negativeBG = '237, 77, 61';

const Badge = styled.div.attrs(({ value }) => ({
  children: value > 99 ? '99+' : value,
}))`
  border: 2px solid ${props => (props.negative ? 'white' : colors.red)};
  background-color: rgba(${props => (props.negative ? negativeBG : normalBG)}, ${props => (props.value ? '1' : '0.6')});
  color: ${props => (props.negative ? 'white' : colors.red)};
  position: absolute;
  font-size: 11px;
  padding: 2px;
  border-radius: 100%;
  line-height: 14px;
  text-align: center;
  min-width: ${props => (props.value ? '14' : '10')}px;
  height: ${props => (props.value ? '14' : '10')}px;
  right: 4px;
  top: 12px;
  font-weight: bold;
`;

Badge.propTypes = {
  value: PropTypes.number,
  negative: PropTypes.bool,
};

Badge.defaultProps = {
  value: null,
  negative: false,
};

export default Badge;
