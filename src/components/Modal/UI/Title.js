import styled from 'styled-components';

const Title = styled.span`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export default Title;
