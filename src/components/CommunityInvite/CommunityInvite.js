import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useTranslation, useInputValue } from 'hooks';
import * as userActions from 'state/users/actions';
import * as authSelectors from 'state/auth/selectors';
import * as communitySelectors from 'state/communities/selectors';
import * as appActions from 'state/app/actions';
import * as communityActions from 'state/communities/actions';

import CommunityHeader from 'containers/Communities/Header';
import Button from 'components/Button';
import PageTitle from 'components/PageTitle';
import { UserPillGrid, UserPill } from 'components/UserPill';
import {
  FlexWrapper,
  FlexInnerWrapper,
  FlexContainer,
  ActionsFooter,
} from 'components/FlexWrapper';

import locales from './i18n';

const SearchInput = styled.input.attrs({
  type: 'search',
})`
  font-size: 16px;
  width: 100%;
  height: 48px;
  line-height: 48px;
  padding: 0 20px;
  margin: 0 0 24px 0;
  border: 1px solid #F5F0F0;
  border-radius: 8px;
  box-sizing: border-box;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.04), 0px 4px 8px rgba(0, 0, 0, 0.08);
  &:focus {
    outline: none;
  }
`;

const CommunityInvite = ({
  communityId,
  close,
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const followingUsers = useSelector(authSelectors.selectFollowingUsers, shallowEqual);
  const slug = useSelector(state => communitySelectors.selectSlugById(state, communityId));
  const name = useSelector(state => communitySelectors.selectName(state, communityId));

  const { t } = useTranslation(locales);

  const [selected, setSelected] = useState([]);
  const search = useInputValue('');
  const [sending, setSending] = useState(false);

  const clickOnUser = userId => () => {
    const index = selected.indexOf(userId);

    if (index > -1) {
      setSelected([
        ...selected.slice(0, index),
        ...selected.slice(index + 1),
      ]);
    } else {
      setSelected([
        ...selected,
        userId,
      ]);
    }
  };

  const skip = () => history.push(`/+${slug}`);

  const filteredContacts = [];
  followingUsers.forEach((userId) => {
    const user = dispatch(userActions.fetchData(userId));

    if (
      user.username.toLowerCase().includes(search.value.toLowerCase())
      || user.displayname.toLowerCase().includes(search.value.toLowerCase())
    ) {
      filteredContacts.push(user);
    }
  });

  const sendInvites = async () => {
    try {
      setSending(true);
      await dispatch(communityActions.sendInvites(communityId, selected));
      dispatch(appActions.addToast(t('Invitations sent')));

      if (close) close();
      else history.push(`/+${slug}`);
    } catch (error) {
      dispatch(appActions.addError(error));
      setSending(false);
    }
  };

  return (
    <FlexWrapper fullHeight mobileHasHeader>
      <FlexInnerWrapper>
        <CommunityHeader communityId={communityId} />
        <PageTitle>{t('invite.to', { name })}</PageTitle>

        <FlexContainer>
          <SearchInput placeholder={t('Search in your contacts')} {...search} />
          <UserPillGrid>
            {filteredContacts.map(user => (
              <UserPill
                key={user.id}
                onClick={clickOnUser(user.id)}
                user={user}
                selected={selected.includes(user.id)}
              />
            ))}
          </UserPillGrid>
        </FlexContainer>
      </FlexInnerWrapper>

      <ActionsFooter>
        <div>
          {close ? (
            <Button
              className="empty"
              onClick={close}
            >
              {t('global:Cancel')}
            </Button>
          ) : (
            <Button
              className="empty"
              onClick={skip}
            >
              {t('Skip')}
            </Button>
          )}
        </div>
        <div>
          <Button
            onClick={sendInvites}
            disabled={selected.length === 0}
            loading={sending}
          >
            {t('invite', { count: selected.length, context: (selected.length === 0 ? 'EMPTY' : '') })}
          </Button>
        </div>
      </ActionsFooter>
    </FlexWrapper>
  );
};

CommunityInvite.propTypes = {
  communityId: PropTypes.string.isRequired,
  close: PropTypes.func,
};

CommunityInvite.defaultProps = {
  close: null,
};

export default CommunityInvite;
