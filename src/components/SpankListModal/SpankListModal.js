import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as replySelectors from 'state/replies/selectors';
import * as threadSelectors from 'state/threads/selectors';
import * as feedSelectors from 'state/feed/selectors';

import Modal from 'components/Modal';
import { Spank, ThumbDown } from 'components/Icons';
import UserLink from 'components/UserLink';
import UserAvatar from 'components/UserAvatar';
import UserDisplayName from 'components/UserDisplayName';

import SpankListWrapper from './SpankListWrapper';
import locales from './i18n';

const getSelector = (type) => {
  switch (type) {
    case 'publication':
      return feedSelectors.publications;
    case 'comment':
      return feedSelectors.comments;
    case 'reply':
      return replySelectors;
    case 'thread':
    default:
      return threadSelectors;
  }
};

const SpankListModal = ({ close, entityId, type }) => {
  const { t } = useTranslation(locales);

  const selector = getSelector(type);
  const spanks = useSelector(state => selector.selectReactionsList(state, entityId), shallowEqual);
  const dislikes = useSelector(state => selector.selectDislikesList(state, entityId), shallowEqual);

  return (
    <Modal
      title={t('global:Reactions')}
      onClose={close}
    >
      <SpankListWrapper>
        <div>
          <div className="reaction-type">
            <Spank />
          </div>
          <div className="reactioners">
            {spanks.reverse().map(userId => (
              <UserLink userId={userId} key={`spank-${entityId}-${userId}`}>
                <UserAvatar size="32px" userId={userId} />
                <UserDisplayName userId={userId} />
              </UserLink>
            ))}
            {!spanks.length && (
              <div className="empty-list">{t('Empty list')}</div>
            )}
          </div>
        </div>

        {['thread', 'reply'].includes(type) && (
          <div>
            <div className="reaction-type">
              <ThumbDown />
            </div>
            <div className="reactioners">
              {dislikes.reverse().map(userId => (
                <UserLink userId={userId} key={`dislike-${entityId}-${userId}`}>
                  <UserAvatar size="32px" userId={userId} />
                  <UserDisplayName userId={userId} />
                </UserLink>
              ))}
              {!dislikes.length && (
                <div className="empty-list">{t('Empty list')}</div>
              )}
            </div>
          </div>
        )}
      </SpankListWrapper>
    </Modal>
  );
};

SpankListModal.propTypes = {
  close: PropTypes.func.isRequired,
  entityId: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['thread', 'reply', 'publication', 'comment']).isRequired,
};

export default withRouter(SpankListModal);
