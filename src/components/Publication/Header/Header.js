import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { useSelector, shallowEqual } from 'react-redux';

import * as feedSelectors from 'state/feed/selectors';

import { Header as HeaderWrapper, UserInfo } from '../UI';
import Avatar from './Avatar';
import AuthorDisplayName from './AuthorDisplayName';
import PrivacyIcon from './PrivacyIcon';
import Actions from './Actions';

const Header = ({ publicationId }) => {
  const createdAt = useSelector(
    state => feedSelectors.publications.selectCreatedAt(state, publicationId),
    shallowEqual,
  );

  return (
    <HeaderWrapper>
      <Avatar publicationId={publicationId} />
      <UserInfo>
        <AuthorDisplayName publicationId={publicationId} />
        <Link to={`/publications/${publicationId}`} className="time">
          {moment(createdAt).fromNow()}
          {' ・ '}
          <PrivacyIcon publicationId={publicationId} />
        </Link>
      </UserInfo>
      <Actions publicationId={publicationId} />
    </HeaderWrapper>
  );
};

Header.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Header.defaultProps = {
};

export default Header;
