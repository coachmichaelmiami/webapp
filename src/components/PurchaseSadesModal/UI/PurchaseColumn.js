import styled from 'styled-components';

const PurchaseColumn = styled.div`
  border: 3px solid ${props => (props.highlighted ? props.theme.colors.main : '#EEE')};
  border-radius: 8px;
  height: ${props => (props.highlighted ? 300 : 250)}px;
  display: flex;
  flex-direction: column;

  .header {
    background: ${props => (props.highlighted ? props.theme.colors.main : '#EEE')};
    width: 150px;
    height: 100px;
    color: ${props => (!props.highlighted ? props.theme.colors.main : 'white')};
    font-weight: 500;
    font-size: 24px;
    text-transform: uppercase;
    display: flex;
    align-items: center;
    justify-content: center;

    @media(max-width: 767px) {
      width: max-content;
      font-size: 16px;
      padding: 0 8px;
      text-align: center;
    }
  }

  .price {
    text-align: center;
    padding: 16px 0;
    flex: 1;

    @media(max-width: 767px) {
      font-size: 14px;
    }
  }

  .action {
    padding: 16px 0 8px;
    text-align: center;

    @media(max-width: 767px) {
      button {
        font-size: 12px;
        padding: 4px 8px;
      }
    }
  }
`;
PurchaseColumn.displayName = 'PurchaseColumn';

export default PurchaseColumn;
