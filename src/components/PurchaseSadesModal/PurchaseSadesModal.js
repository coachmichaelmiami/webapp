import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useTranslation, useOpenClose } from 'hooks';

import Modal from 'components/Modal';
import Warning from 'components/Warning';

import mercadopago from './img/mercadopago.png';
import paypal from './img/paypal.png';
import Mercadopago from './Mercadopago';
import PayPal from './PayPal';
import Processors from './UI/Processors';
import Title from './UI/Title';
import SuccessAnimation from './UI/SuccessAnimation';
import locales from './i18n';

const PurchaseSadesModal = ({
  insufficient,
  onClose,
  onCancel,
  ...props
}) => {
  const { t } = useTranslation(locales);

  const [processor, setProcessor] = useState(false);
  const [isSuccessModalOpen, openSuccessModal] = useOpenClose();

  const selectProcessor = useCallback(newProcessor => () => {
    setProcessor(newProcessor);
  }, []);

  const done = useCallback((showSuccess) => {
    if (!showSuccess) {
      if (onCancel) onCancel();
      else if (onClose) onClose();
    } else {
      openSuccessModal();
    }
  }, [onCancel, onClose, openSuccessModal]);

  if (isSuccessModalOpen) {
    return (
      <Modal zIndex={1500} {...props} onClose={onCancel || onClose}>
        <SuccessAnimation />
      </Modal>
    );
  }

  return (
    <Modal zIndex={1500} {...props} title={t('Purchase Sades')} onCancel={onCancel || onClose}>
      {insufficient && (
        <Warning>{t('You have insufficient Sades. Do you want to purchase more and be able to perform this action?')}</Warning>
      )}

      {processor === 'mercadopago' && <Mercadopago done={done} />}
      {processor === 'paypal' && <PayPal done={done} />}

      {!processor && (
        <div>
          <Title>{t('Payment options')}</Title>

          <Processors>
            <div onClick={selectProcessor('mercadopago')} role="button" tabIndex={-1} onKeyDown={selectProcessor('mercadopago')}>
              <img src={mercadopago} alt="Mercado Pago" />
              <div className="flag">{t('Only argentine peso')}</div>
            </div>

            <div onClick={selectProcessor('paypal')} role="button" tabIndex={-1} onKeyDown={selectProcessor('paypal')}>
              <img src={paypal} alt="PayPal" />
            </div>
          </Processors>
        </div>
      )}
    </Modal>
  );
};

PurchaseSadesModal.propTypes = {
  insufficient: PropTypes.bool,
  onClose: PropTypes.func,
  onCancel: PropTypes.func,
};

PurchaseSadesModal.defaultProps = {
  insufficient: false,
  onClose: null,
  onCancel: null,
};

export default PurchaseSadesModal;
