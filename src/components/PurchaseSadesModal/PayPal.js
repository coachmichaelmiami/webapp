import React, {
  useCallback, useState, useRef, useEffect,
} from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';
import * as bankActions from 'state/bank/actions';

import Button from 'components/Button';
import Loading from 'components/Loading';

import Columns from './UI/Columns';
import PurchaseColumn from './UI/PurchaseColumn';
import locales from './i18n';

const Mercadopago = ({ done }) => {
  const { t } = useTranslation(locales);
  const paymentContainer = useRef(null);

  const [order, setOrder] = useState(null);

  useEffect(() => {
    if (paymentContainer.current && order && order !== true) {
      const script = document.createElement('script');
      script.src = 'https://www.paypal.com/sdk/js?client-id=ATGYwzCgJUCX-SUs9T--gGCa4lK_6YXX8fDu39u3zy24ApFaOVI-qvUt51W2v9Lq61wSIKbCAvRrUt-J';
      script.onload = () => {
        // eslint-disable-next-line no-undef
        paypal.Buttons({
          createOrder() {
            return order.paypal.order.id;
          },
          onApprove: async () => {
            const orderId = order.id;
            setOrder(true);
            await bankActions.capturePaypalOrder(orderId);
            done(true);
          },
        }).render('#paypal-button-container');
      };
      paymentContainer.current.appendChild(script);
    }
  }, [order, paymentContainer, done]);

  const buy = useCallback(itemName => async () => {
    try {
      setOrder(true);
      const data = await bankActions.createOrder(itemName, {}, 'PAYPAL');
      setOrder(data);
    } catch (error) {
      setOrder(null);
    }
  }, []);

  if (order === true) return <Loading />;
  if (order) {
    return (
      <div>
        <div ref={paymentContainer} />
        <div id="paypal-button-container" />
      </div>
    );
  }

  return (
    <Columns>
      <PurchaseColumn>
        <div className="header">{t('{{amount}} Sades', { amount: 50 })}</div>
        <div className="price">
          {t('Price')}
          <br />
          <strong>{t('{{price}} usd', { price: 4.99 })}</strong>
        </div>
        <div className="action"><Button color="white" fontColor="black" onClick={buy('SADES_PACK_50')}>{t('global:Buy')}</Button></div>
      </PurchaseColumn>

      <PurchaseColumn highlighted>
        <div className="header">{t('{{amount}} Sades', { amount: 100 })}</div>
        <div className="price">
          {t('Price')}
          <br />
          <strong>{t('{{price}} usd', { price: 8.49 })}</strong>
        </div>
        <div className="action"><Button onClick={buy('SADES_PACK_100')}>{t('global:Buy')}</Button></div>
      </PurchaseColumn>

      <PurchaseColumn>
        <div className="header">{t('{{amount}} Sades', { amount: 320 })}</div>
        <div className="price">
          {t('Price')}
          <br />
          <strong>{t('{{price}} usd', { price: 22.99 })}</strong>
        </div>
        <div className="action"><Button color="white" fontColor="black" onClick={buy('SADES_PACK_320')}>{t('global:Buy')}</Button></div>
      </PurchaseColumn>
    </Columns>
  );
};

Mercadopago.propTypes = {
  done: PropTypes.func.isRequired,
};

Mercadopago.defaultProps = {
};

export default Mercadopago;
