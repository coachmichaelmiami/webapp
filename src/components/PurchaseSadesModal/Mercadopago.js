import React, {
  useCallback, useState, useRef, useEffect,
} from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';
import * as bankActions from 'state/bank/actions';

import Button from 'components/Button';
import Loading from 'components/Loading';

import Columns from './UI/Columns';
import PurchaseColumn from './UI/PurchaseColumn';
import locales from './i18n';

const Mercadopago = ({ done }) => {
  const { t } = useTranslation(locales);
  const paymentContainer = useRef(null);

  const [order, setOrder] = useState(null);

  useEffect(() => {
    if (paymentContainer.current && order && order !== true) {
      const script = document.createElement('script');
      script.src = 'https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js';
      script.setAttribute('data-preference-id', order.mercadopago.preference.id);
      script.onload = () => {
        const button = document.querySelector('.mercadopago-button');
        if (button) {
          button.click();
          done();
        }
      };
      paymentContainer.current.appendChild(script);
    }
  }, [order, paymentContainer, done]);

  const buy = useCallback(itemName => async () => {
    try {
      setOrder(true);
      const data = await bankActions.createOrder(itemName, {}, 'MERCADOPAGO');
      setOrder(data);
    } catch (error) {
      setOrder(null);
    }
  }, []);

  if (order === true) return <Loading />;
  if (order) return <div ref={paymentContainer} />;

  return (
    <Columns>
      <PurchaseColumn>
        <div className="header">{t('{{amount}} Sades', { amount: 50 })}</div>
        <div className="price">
          {t('Price')}
          <br />
          <strong>{t('{{price}} pesos', { price: 350 })}</strong>
        </div>
        <div className="action"><Button color="white" fontColor="black" onClick={buy('SADES_PACK_50')}>{t('global:Buy')}</Button></div>
      </PurchaseColumn>

      <PurchaseColumn highlighted>
        <div className="header">{t('{{amount}} Sades', { amount: 100 })}</div>
        <div className="price">
          {t('Price')}
          <br />
          <strong>{t('{{price}} pesos', { price: 680 })}</strong>
        </div>
        <div className="action"><Button onClick={buy('SADES_PACK_100')}>{t('global:Buy')}</Button></div>
      </PurchaseColumn>

      <PurchaseColumn>
        <div className="header">{t('{{amount}} Sades', { amount: 320 })}</div>
        <div className="price">
          {t('Price')}
          <br />
          <strong>{t('{{price}} pesos', { price: 2000 })}</strong>
        </div>
        <div className="action"><Button color="white" fontColor="black" onClick={buy('SADES_PACK_320')}>{t('global:Buy')}</Button></div>
      </PurchaseColumn>
    </Columns>
  );
};

Mercadopago.propTypes = {
  done: PropTypes.func.isRequired,
};

Mercadopago.defaultProps = {
};

export default Mercadopago;
