import { useEffect } from 'react';

// eslint-disable-next-line
const useOnMount = mount => useEffect(mount, []);

export default useOnMount;
