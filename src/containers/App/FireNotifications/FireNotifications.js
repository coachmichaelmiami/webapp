import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import MatchModal from './MatchModal';
import SuperFireModal from './SuperFireModal';

const FireNotifications = () => {
  const match = useSelector(authSelectors.getFireMatch, shallowEqual);
  const superfire = useSelector(authSelectors.getSuperFire, shallowEqual);

  if (match) return <MatchModal fire={match} />;
  if (superfire) return <SuperFireModal fire={superfire} />;

  return null;
};

FireNotifications.propTypes = {
};

FireNotifications.defaultProps = {
};

export default FireNotifications;
