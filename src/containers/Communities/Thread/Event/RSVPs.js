import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';

import UserDisplayName from 'components/UserDisplayName';
import UserLink from 'components/UserLink';

import locales from '../i18n';

const Wrapper = styled.div`
  border-top: 1px solid ${props => props.theme.colors.secondaryText};

  h3 {
    font-size: 16px;
    color: #333;
  }

  .rsvps {
    a {
      font-size: 14px;
      color: #666;

      &:hover {
        color: ${props => props.theme.colors.main};
      }
    }
  }

  @media(max-width: 768px) {
    padding: 0 16px;
  }
`;
Wrapper.displayName = 'Wrapper';

const RSVPs = ({ eventId }) => {
  const { t } = useTranslation(locales);

  const userIds = useSelector(
    state => eventSelectors.selectRsvpUserIds(state, eventId),
    shallowEqual,
  );
  const rsvpCount = useSelector(state => eventSelectors.selectRsvpCount(state, eventId));

  if (!rsvpCount) return null;

  return (
    <Wrapper>
      <h3>{t('RSVPs ({{count}})', { count: rsvpCount })}</h3>

      <div className="rsvps">
        {userIds.map((userId, index) => (
          <span key={`event-${eventId}-rsvp-${userId}`}>
            <UserLink userId={userId}><UserDisplayName userId={userId} /></UserLink>
            {index < (userIds.length - 1) && ', '}
          </span>
        ))}
      </div>
    </Wrapper>
  );
};

RSVPs.propTypes = {
  eventId: PropTypes.string.isRequired,
};

RSVPs.defaultProps = {
};

export default RSVPs;
