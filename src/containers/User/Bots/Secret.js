import styled from 'styled-components';

const Secret = styled.div`
  font-size: 24px;
  font-weight: 500;
  background: #EEE;
  padding: 16px;
  margin: 48px 0;
`;
Secret.displayName = 'Secret';

export default Secret;
