import styled from 'styled-components';

const Wrapper = styled.div`
  h3 {
    margin: 32px 0 0;
  }

  table {
    width: 100%;
  }

  td, th {
    padding: 4px 8px;
    min-width: 100px;
  }

  th {
    &:first-child {
      width: 100%;
    }

    font-weight: 500;
    color: #333;
  }

  tbody tr:nth-child(2n) {
    background-color: ${props => props.theme.colors.mainLight};
  }

  .actions {
    position: sticky;
    bottom: 0;
    background: white;
    padding: 16px 0;
    text-align: right;
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;
