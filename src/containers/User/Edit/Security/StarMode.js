import React, { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Toggle from 'components/Toggle';

import locales from '../i18n';

const StarMode = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const active = useSelector(authSelectors.selectStarMode);

  const [loading, setLoading] = useState(false);

  const onChange = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(authActions.updateStarMode(!active));
      dispatch(appActions.addToast(t('Star Mode updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
  }, [dispatch, t, active]);

  return (
    <div>
      <h2>{t('Star Mode')}</h2>

      <Toggle active={active} label={t('Active')} onChange={onChange} loading={loading} position="left" />
    </div>
  );
};

StarMode.propTypes = {
};

StarMode.defaultProps = {
};

export default StarMode;
