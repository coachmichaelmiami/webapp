import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import TrashCan from 'components/Icons/TrashCan';
import UserDisplayName from 'components/UserDisplayName';
import Button from 'components/Button';
import UserLink from 'components/UserLink';
import UserAvatar from 'components/UserAvatar';

const Row = styled.tr`
  > td:first-child {
    a {
      display: flex;
      align-items: center;
      color: ${props => props.theme.colors.main};

      .avatar {
        margin-right: 8px;
      }
    }
  }

  > td:last-child {
    width: 120px;
    text-align: right;

    button svg {
      width: 20px;
      height: 20px;

      path {
        fill: ${props => props.theme.colors.secondary};
      }
    }
  }
`;
Row.displayName = 'Row';

const OrganizationAdmin = ({ userId }) => {
  const dispatch = useDispatch();

  const [removing, setRemoving] = useState(false);

  const remove = useCallback(async () => {
    try {
      setRemoving(true);
      await dispatch(authActions.removeOrganizationAdmin(userId));
    } catch (error) {
      setRemoving(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, userId]);

  return (
    <Row>
      <td>
        <UserLink userId={userId}>
          <UserAvatar userId={userId} size="24px" />
          <UserDisplayName userId={userId} />
        </UserLink>
      </td>
      <td><Button color="white" fontColor="black" onClick={remove} loading={removing}><TrashCan outline /></Button></td>
    </Row>
  );
};

OrganizationAdmin.propTypes = {
  userId: PropTypes.number.isRequired,
};

OrganizationAdmin.defaultProps = {
};

export default OrganizationAdmin;
