import React, { useState, useCallback, useMemo } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Trans } from 'react-i18next';
import shortid from 'shortid';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import UserTag from 'components/UserTag';
import Button from 'components/Button';
import PurchaseButton from 'components/PurchaseButton';
import PurchaseModal from 'components/PurchaseButton/PurchaseModal';

import { USERTAGS } from '../../../../constants';
import locales from '../i18n';

const Explanation = styled.div`
  font-size: 16px;
  margin-bottom: 32px;

  > div {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 16px;

    > span {
      font-weight: 500;
      font-size: 18px;
    }
  }
`;
Explanation.displayName = 'Explanation';

const Wrapper = styled.div`
  margin: 32px !important;

  .usertags {
    .tag-group {
      margin-bottom: 16px;
      flex-wrap: wrap;
      font-size: 14px;
      display: flex;

      > div {
        margin: 0 4px 4px;
      }
    }
  }
`;
Wrapper.displayName = 'TagsWrapper';

const Counter = styled.div`
  font-size: 14px;
  margin-top: 16px;
  margin-bottom: 16px;
  color: ${props => props.theme.colors.secondary};
`;
Counter.displayName = 'Counter';

const Tags = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const userTags = useSelector(authSelectors.selectTags, shallowEqual);
  const username = useSelector(authSelectors.selectUsername);
  const available = useSelector(authSelectors.tagsAvailable);
  const purchased = useSelector(authSelectors.tagsPurchased);

  const [tags, setTags] = useState(userTags);
  const [saving, setSaving] = useState(false);
  const [purchase, setPurchase] = useState(null);
  const [purchaseSlot, setPurchaseSlot] = useState(null);

  const closePurchase = useCallback(() => setPurchase(null), []);

  const toggleSelect = useCallback(tagkey => () => {
    const tag = USERTAGS[tagkey].label;
    setTags((currentTags) => {
      if (currentTags.includes(tag)) return currentTags.filter(tt => tt !== tag);

      if (USERTAGS[tagkey].price > 0 && !purchased.includes(tag)) {
        setPurchase(tag);
        return currentTags;
      }

      if (currentTags.length >= available) {
        setPurchaseSlot(tag);
        return currentTags;
      }

      return [
        ...currentTags,
        tag,
      ];
    });
  }, [available, purchased]);

  const addTagAfterPurchase = useCallback(() => {
    setTags(currentTags => [...currentTags, purchase, purchaseSlot]);
    setPurchase(null);
    setPurchaseSlot(null);
  }, [purchase, purchaseSlot]);

  const save = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.updateTags(tags));
      history.replace(`/@${username}`);
      dispatch(appActions.addToast(t('Tags updated')));
    } catch (e) {
      setSaving(false);
      dispatch(appActions.addError(e));
    }
  }, [dispatch, history, t, tags, username]);

  const groups = useMemo(() => {
    const res = {};

    Object.keys(USERTAGS).forEach((tagkey) => {
      const tag = USERTAGS[tagkey];
      if (!res[tag.group]) res[tag.group] = [];
      res[tag.group].push({ ...tag, key: tagkey });
    });

    return res;
  }, []);

  const saveDisabled = tags.length === userTags.length && tags.every(tt => userTags.includes(tt));

  return (
    <>
      <Wrapper>
        <Explanation>
          <Trans t={t} i18nKey="tags.explanation" ns="userEdit" values={{ available }}>
            <span>
              Choose the tags that you feel define your sexuality.
              You can change them as many times as you want.
            </span>

            <div>
              <span>{'Available slots: {{available}}'}</span>
              <PurchaseButton
                itemName="TAG_SLOT"
                component={<Button>{t('Add More')}</Button>}
              />
            </div>
          </Trans>
        </Explanation>

        <div className="usertags">
          {Object.values(groups).map(group => (
            <div className="tag-group" key={`taggroup-${shortid.generate()}`}>
              {Object.values(group).map(tag => (
                <UserTag
                  key={`usertag-general-${tag.label}`}
                  dark
                  selected={tags.includes(tag.label)}
                  onClick={toggleSelect(tag.key)}
                  role="button"
                  highlighted={tag.price > 0 && !purchased.includes(tag.label)}
                >
                  {t(`global:TAG.${tag.label}`)}
                </UserTag>
              ))}
            </div>
          ))}
        </div>

        <Counter>
          {t('Selected tags {{selected}}', { selected: tags.length })}
        </Counter>

        <Button onClick={save} disabled={saveDisabled} loading={saving}>{t('global:Save')}</Button>
      </Wrapper>

      {/* Modals */}
      {purchase && (
        <PurchaseModal
          itemName={`USERTAG.${purchase}`}
          otherItems={tags.length >= available ? ['TAG_SLOT'] : undefined}
          close={closePurchase}
          afterPurchase={addTagAfterPurchase}
        />
      )}

      {purchaseSlot && (
        <PurchaseModal itemName="TAG_SLOT" close={closePurchase} afterPurchase={addTagAfterPurchase} />
      )}
    </>
  );
};

Tags.propTypes = {
};

Tags.defaultProps = {
};

export default Tags;
