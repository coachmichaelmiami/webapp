import React, { useCallback } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';

import { useTranslation } from 'hooks';

import Button from 'components/Button';
import PurchaseSadesModal from 'components/PurchaseSadesModal';

import locales from './i18n';

const BuyMore = () => {
  const history = useHistory();
  const { t } = useTranslation(locales);

  const isModalOpen = useRouteMatch('/user/sades/buy');

  const closeModal = useCallback(() => {
    history.push('/user/sades');
  }, [history]);

  return (
    <>
      <Button to="/user/sades/buy">{t('Buy more')}</Button>

      {isModalOpen && (
        <PurchaseSadesModal onCancel={closeModal} />
      )}
    </>
  );
};

BuyMore.propTypes = {
};

BuyMore.defaultProps = {
};

export default BuyMore;
