import styled from 'styled-components';

const HeaderContainer = styled.div`
  margin: 0 48px;
  display: flex;
  flex-direction: column;

  .balance {
    margin: 0 auto;
    font-size: 24px;
    box-shadow: inset 0 0 6px 0px rgb(0 0 0 / 20%);
    padding: 16px;
    margin-bottom: 16px;
    border-radius: 8px;
    background-color: ${props => props.theme.colors.mainLight};
  }

  .new-transfer {
    display: flex;
    justify-content: space-between;
    padding: 4px 0;

    @media(max-width: 767px) {
      flex-direction: column;
    }
  }
`;
HeaderContainer.displayName = 'HeaderContainer';

export default HeaderContainer;
