import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import UserTag from 'components/UserTag';

import locales from '../i18n';

const UserTagsWrapper = styled.div.attrs({
  className: 'usertags',
})`
  margin-bottom: 22px !important;
  flex-wrap: wrap;
`;
UserTagsWrapper.displayName = 'UserTagsWrapper';

const UserTags = ({ userId, dark }) => {
  const { t } = useTranslation(locales);

  const isOrganization = useSelector(userSelectors.isOrganization(userId));
  const tags = useSelector(userSelectors.getTags(userId));
  const pronoun = useSelector(userSelectors.getPronoun(userId));

  if (isOrganization || !tags || !tags.length) return null;

  return (
    <UserTagsWrapper>
      {tags.map(tag => (
        <UserTag key={`usertag-${userId}-${tag}`} dark={dark}>
          {t(`global:TAG.${tag}`, { context: pronoun })}
        </UserTag>
      ))}
    </UserTagsWrapper>
  );
};

UserTags.propTypes = {
  userId: PropTypes.number.isRequired,
  dark: PropTypes.bool,
};

UserTags.defaultProps = {
  dark: false,
};

export default UserTags;
