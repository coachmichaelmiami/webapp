import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as appActions from 'state/app/actions';
import * as appSelectors from 'state/app/selectors';

import { AccountDetails } from 'components/Icons';
import { Action } from 'components/Header';

const ShowParticipantsButton = () => {
  const dispatch = useDispatch();
  const showing = useSelector(appSelectors.selectIsShowingParticipants);

  const toggleShowParticipants = () => dispatch(appActions.setShowingParticipants(!showing));

  return (
    <Action onClick={toggleShowParticipants} pressed={showing} mobileOnly>
      <AccountDetails color="#666" />
    </Action>
  );
};

export default ShowParticipantsButton;
