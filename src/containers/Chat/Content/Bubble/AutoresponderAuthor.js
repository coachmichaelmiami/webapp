import styled from 'styled-components';

const AutoresponderAuthor = styled.div`
  font-weight: bold;
  font-size: 12px;
`;
AutoresponderAuthor.displayName = 'AutoresponderAuthor';

export default AutoresponderAuthor;
