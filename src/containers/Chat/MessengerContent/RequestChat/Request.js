import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import { Trans } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import * as messengerSelectors from 'state/messengers/selectors';
import * as userSelectors from 'state/users/selectors';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';
import * as messengerActions from 'state/messengers/actions';

import { useTranslation, useInputValue, useOnMount } from 'hooks';

import Button from 'components/Button';
import EmptyState from 'components/EmptyState';
import { FlexWrapper, FlexInnerWrapper } from 'components/FlexWrapper';

import OnboardingModal from '../OnboardingModal';
import { EMAILS } from '../../../../constants';
import locales from '../../i18n';

const Textarea = styled.textarea`
  flex: 1;
  outline: none;
  resize: none;
  margin: 40px 0 20px;
  width: 100%;
  height: 120px;
  padding: 10px;
  font-size: 15px;
  border: 1px solid #eee;
  box-sizing: border-box;
  font-family: 'Roboto',sans-serif;
`;

const RequestsLeft = styled.div`
  color: ${props => props.theme.colors.secondaryText};
  margin-top: 16px;
`;
RequestsLeft.displayName = 'RequestsLeft';

const RequestChat = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const requestEl = useRef(null);
  const { chatId } = useParams();

  const userIsConfirmed = useSelector(authSelectors.isConfirmed);
  const email = useSelector(authSelectors.selectEmail);

  const userId = useSelector(state => messengerSelectors.getUserId(state, chatId));
  const participant = useSelector(userSelectors.getById(userId), shallowEqual);
  const requestsLeft = useSelector(authSelectors.getRequestsLeft);

  const [loading, setLoading] = useState(false);
  const requestMessage = useInputValue('');

  useOnMount(() => {
    if (requestEl.current) requestEl.current.focus();
  });

  const sendRequest = async () => {
    try {
      setLoading(true);
      await dispatch(messengerActions.request(chatId, requestMessage.value));
    } catch (error) {
      if (error.response.data.errorCode === 400) dispatch(appActions.addError(t('Message can\'t be empty')));
      else dispatch(appActions.addError(error));

      setLoading(false);
    }
  };

  if (!userIsConfirmed) {
    return (
      <EmptyState
        title={t('You need to confirm your account')}
        subtitle={(
          <Trans t={t} i18nKey="emailconfirm" ns="chatDashboard" values={{ email, ouremail: EMAILS.CONFIRMATIONS }}>
            <p>
              {'You can\'t contact other people in private until you e-mail is confirmed with us'}
            </p>
            <p>
              {'We\'ve '}
              sent you the instructions to
              {' '}
              <b>your address</b>
              . If you
              {' didn\'t '}
              receive our e-mail, please write us to
              {' '}
              <b>our address</b>
              .
            </p>
          </Trans>
        )}
      />
    );
  }

  return (
    <>
      <FlexWrapper mobileHasHeader canOverflow>
        <FlexInnerWrapper>
          <EmptyState
            title={t('Request for chat!')}
            subtitle={(
              <Trans
                t={t}
                i18nKey="chat.request"
                ns="chatDashboard"
                tOptions={{ context: participant.pronoun, requestsLeft }}
              >
                You need to send a request to this person to start a conversation.
                <br />
                You can send a message explaining why you want to chat with them, but be mindful:
                you only have one shot!

                <RequestsLeft>{'{{requestsLeft}} available requests left'}</RequestsLeft>

                <Textarea ref={requestEl} placeholder={t('Type a message')} {...requestMessage} />
                <br />

                Remember the three basic rules to establish contact:
                {' '}
                Show consideration! Show respect! Show interest!

                <br />
                <br />

                <Button
                  loading={loading}
                  disabled={!requestMessage.value.length}
                  onClick={sendRequest}
                >
                  Send
                </Button>
              </Trans>
            )}
          />
        </FlexInnerWrapper>
      </FlexWrapper>

      {/* Modals */}
      <OnboardingModal />
    </>
  );
};

RequestChat.propTypes = {
};

export default RequestChat;
