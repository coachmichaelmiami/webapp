import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import { Item } from 'components/Menu';
import CircleMultiple from 'components/Icons/CircleMultiple';

import locales from '../../i18n';

const SadesContainer = styled(Item)`
  display: flex;
  justify-content: space-between;

  > div:first-child {
    display: flex;

    > a {
      line-height: normal !important;
    }

    > svg {
      width: 20px !important;
      height: 20px;
      margin-right: 4px;

      path {
        fill: ${props => props.theme.colors.secondary};
      }
    }
  }

  > div:last-child {
    font-size: 12px;
    font-weight: bold;
    background-color: ${props => props.theme.colors.mainLight};
    padding: 4px 8px;
    border-radius: 8px;

    &:after {
      content: ' §';
    }
  }
`;
SadesContainer.displayName = 'SadesContainer';

const Sades = () => {
  const { t } = useTranslation(locales);

  const sades = useSelector(authSelectors.getSades);

  return (
    <SadesContainer>
      <div>
        <CircleMultiple />
        <Link to="/user/sades">{t('global:Sades')}</Link>
      </div>
      <div>{sades.toFixed(2)}</div>
    </SadesContainer>
  );
};

Sades.propTypes = {
};

Sades.defaultProps = {
};

export default Sades;
