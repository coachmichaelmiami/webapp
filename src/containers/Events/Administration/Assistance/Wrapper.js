import styled from 'styled-components';

const Wrapper = styled.div`
  .counter {
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 32px;

    input {
      max-width: 300px;
      border-bottom: 1px solid #EEE;
    }
  }

  table {
    width: 100%;

    tbody {
      tr:nth-child(2n) {
        background-color: ${props => props.theme.colors.mainLight};
      }

      td {
        padding: 8px;

        &.unassisted {
          opacity: .5;
        }

        &:nth-child(2) {
          text-align: right;
        }
      }

      .userrow {
        display: flex;

        .avatar {
          margin-right: 8px;
        }

        .displayname {
          font-weight: bold;
        }

        .username {
          color: #AAA;
          font-size: 12px;
        }
      }
    }
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;
