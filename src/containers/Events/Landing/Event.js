import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import * as eventSelectors from 'state/events/selectors';

import { SITE_URL } from '../../../constants';

const Event = ({ eventId }) => {
  const flyer = useSelector(state => eventSelectors.selectFlyer(state, eventId));
  const name = useSelector(state => eventSelectors.selectName(state, eventId));
  const link = useSelector(state => eventSelectors.selectAnnouncementLink(state, eventId));

  return (
    <div>
      <Link to={(link || '').replace(SITE_URL, '')}>
        <img src={flyer} alt={name} height={258} width={193} />
      </Link>
    </div>
  );
};

Event.propTypes = {
  eventId: PropTypes.string.isRequired,
};

Event.defaultProps = {
};

export default Event;
