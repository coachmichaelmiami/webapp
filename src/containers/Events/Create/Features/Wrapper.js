import styled from 'styled-components';

const Wrapper = styled.div`
  h3 {
    color: #666;
    margin: 0 0 16px;
    font-weight: 300;
  }

  > div {
    margin-bottom: 32px;

    p {
      font-size: 14px;
      color: ${props => props.theme.colors.secondary};
    }
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;
