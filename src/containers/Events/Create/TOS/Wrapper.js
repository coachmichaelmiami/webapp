import styled from 'styled-components';

const Wrapper = styled.div`
  ul {
    list-style: disc;
    margin-left: 32px;

    li {
      margin-bottom: 16px;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;
