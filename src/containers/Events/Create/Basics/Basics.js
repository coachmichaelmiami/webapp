import React, {
  useState, useCallback, useRef, useEffect,
} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { useTranslation, useOpenClose, useInputValue } from 'hooks';

import Input from 'components/Forms/Input';
import Toggle from 'components/Toggle';
import Datepicker from 'components/Datepicker';
import Button from 'components/Button';
import Modal from 'components/Modal';

import Wrapper from './Wrapper';
import locales from '../i18n';

const Basics = ({
  isPublic, setIsPublic, showLocation, setShowLocation, date,
  setDate, name, time, price, sponsors, setSponsors,
}) => {
  const { t } = useTranslation(locales);
  const nameElement = useRef(null);

  const [isFocused, setIsFocused] = useState(false);
  const [isopenAddSponsor, openAddSponsor, closeAddSponsor] = useOpenClose(false);
  const sponsorName = useInputValue('');
  const sponsorLink = useInputValue('');

  useEffect(() => {
    if (nameElement.current) {
      nameElement.current.focus();
    }
  }, []);

  useEffect(() => {
    if (!isopenAddSponsor && (sponsorName.value.length || sponsorLink.value.length)) {
      sponsorName.change('');
      sponsorLink.change('');
    }
  }, [isopenAddSponsor, sponsorName, sponsorLink]);

  const onFocusChange = useCallback(({ focused }) => setIsFocused(focused), []);
  const isOutsideRange = useCallback(d => moment().diff(d, 'days') > 0, []);
  const initialVisibleMonth = useCallback(() => moment().add(7, 'days'), []);

  const togglePublic = useCallback((e) => {
    setIsPublic(e.target.checked);
  }, [setIsPublic]);

  const toggleShowLocation = useCallback((e) => {
    setShowLocation(!e.target.checked);
  }, [setShowLocation]);

  const addSponsor = useCallback(() => {
    setSponsors(cv => [
      ...cv,
      { name: sponsorName.value, link: sponsorLink.value },
    ]);

    closeAddSponsor();
  }, [setSponsors, sponsorName.value, sponsorLink.value, closeAddSponsor]);

  const removeSponsor = useCallback(index => () => {
    setSponsors(cv => [
      ...cv.slice(0, index),
      ...cv.slice(index + 1),
    ]);
  }, [setSponsors]);

  return (
    <>
      <Wrapper>
        <h3>{t('Event name')}</h3>
        <Input {...name} ref={nameElement} />

        <h3>{t('Date and Time')}</h3>
        <div className="datetime">
          <Datepicker
            id="create-event-date"
            date={date}
            onDateChange={setDate}
            focused={isFocused}
            onFocusChange={onFocusChange}
            required
            keepOpenOnDateSelect={false}
            numberOfMonths={1}
            showClearDate
            block
            hideKeyboardShortcutsPanel
            isOutsideRange={isOutsideRange}
            initialVisibleMonth={initialVisibleMonth}
            monthSelection={false}
          />

          <input type="time" {...time} className="timepicker" />
        </div>

        <h3>{t('Ticket Price')}</h3>
        <div className="price">
          <Input type="number" {...price} />
          <div>ARS</div>
        </div>

        <h3>{t('Options')}</h3>
        <div className="options">
          <Toggle label={t('The Event is in a public space (bar, cafe, etc.)')} active={isPublic} onChange={togglePublic} position="left" />

          <Toggle label={t('Show location after RSVP')} active={!showLocation} onChange={toggleShowLocation} position="left" />
        </div>

        <h3>{t('Sponsors')}</h3>
        <div className="sponsors">
          {sponsors.map((sponsor, index) => (
            <div className="sponsor" key={`sponsor-${sponsor.name}`}>
              <strong>{sponsor.name}</strong>
              {': '}
              <span>{sponsor.link}</span>
              <Button className="empty" onClick={removeSponsor(index)}>{t('global:Remove')}</Button>
            </div>
          ))}
          <Button className="empty" onClick={openAddSponsor}>{t('global:Add')}</Button>
        </div>
      </Wrapper>

      {/* Modals */}
      {isopenAddSponsor && (
        <Modal
          title={t('Add sponsor')}
          onCancel={closeAddSponsor}
          actions={[
            <Button key="add-sponsor" onClick={addSponsor}>{t('global:Add')}</Button>,
          ]}
        >
          <Input {...sponsorName} placeholder={t('Name')} />
          <Input {...sponsorLink} placeholder={t('Link')} />
        </Modal>
      )}
    </>
  );
};

Basics.propTypes = {
  isPublic: PropTypes.bool.isRequired,
  setIsPublic: PropTypes.func.isRequired,
  showLocation: PropTypes.bool.isRequired,
  setShowLocation: PropTypes.func.isRequired,
  date: PropTypes.instanceOf(moment).isRequired,
  setDate: PropTypes.func.isRequired,
  name: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  time: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  price: PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  sponsors: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
  })).isRequired,
  setSponsors: PropTypes.func.isRequired,
};

Basics.defaultProps = {
};

export default Basics;
