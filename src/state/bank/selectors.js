import { createSelector } from 'reselect';

// Input selectors
const selectCurrentPeriod = state => state.bank.periods.current;
//

export const selectGeneralTax = createSelector(
  selectCurrentPeriod,
  period => period?.taxes?.general || 0,
);

export const selectBuyableItemPrice = createSelector(
  selectCurrentPeriod,
  (_, name) => name,
  (period, name) => period?.buyableItems?.[name]?.price || null,
);

export const selectBuyableItemPriceBulk = createSelector(
  selectCurrentPeriod,
  (_, names) => names,
  (period, names) => names.reduce((acc, cv) => (period?.buyableItems?.[cv]?.price || 0) + acc, 0),
);
