import produce from 'immer';

import {
  LOAD_PERIOD,
  WIPE,
} from './constants';

export const initialState = {
  periods: {
    current: null,
  },
};

const reducer = (state = initialState, action) => produce(state, (draft) => {
  switch (action.type) {
    case LOAD_PERIOD:
      draft.periods.current = action.data;
      break;

    case WIPE:
      draft.periods = initialState.periods;
      break;

    default:
  }
});

export default reducer;
