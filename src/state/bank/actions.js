import ReactGA from 'react-ga';

import Api from 'state/api';

import { LOAD_PERIOD } from './constants';

export const loadCurrentPeriod = data => (dispatch) => {
  dispatch({ type: LOAD_PERIOD, data });
};

export const createTransaction = async ({
  to, amount, concept, data,
}) => {
  const { data: transaction } = await Api.req.post('/bank/transactions', {
    to, amount, concept, data,
  });

  ReactGA.event({
    category: 'Bank',
    action: 'New Transaction',
    label: `Total: ${transaction.amount + transaction.tax}`,
  });

  return transaction;
};

export const createPurchase = async ({ item, payload }) => {
  const { data: transaction } = await Api.req.post('/bank/transactions/purchase', { item, data: payload });

  ReactGA.event({
    category: 'Bank',
    action: 'New Purchase',
    label: item,
  });

  return transaction;
};

export const createOrder = async (itemName, itemData, paymentMethod) => {
  const { data } = await Api.req.post('/bank/orders', { itemName, itemData, paymentMethod });

  ReactGA.event({
    category: 'Bank',
    action: 'New Order',
    label: data.item.name,
    value: data.invoice.total,
  });

  return data;
};

export const capturePaypalOrder = async (orderId) => {
  await Api.req.post(`/bank/orders/paypal/capture/${orderId}`);
};
