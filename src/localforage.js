/* eslint-disable import/prefer-default-export */
import localforage from 'localforage';

export const notificationSettings = localforage.createInstance({
  storeName: 'notification-settings',
});
